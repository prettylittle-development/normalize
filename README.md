# normalize.css v7.0.0

The `normalize.css` module is an exact clone of normalize.css.
normalize.css <q>makes browsers render all elements more consistently and in
line with modern standards</q>. It is developed and maintained by [Nicolas
Gallagher](https://twitter.com/necolas).

* Project homepage: [necolas.github.io/normalize.css](http://necolas.github.io/normalize.css/)
* GitHub repo: [github.com/necolas/normalize.css](https://github.com/necolas/normalize.css/)
